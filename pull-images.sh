#!/bin/bash
# Pulls images from the official Docker repository.
# The list of images is defined in the corresponding .list file.

mapfile -t images < $(basename "$0" .sh).list
for image in "${images[@]}"
do
    docker pull "$image"
done

docker system prune --force
