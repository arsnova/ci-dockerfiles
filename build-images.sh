#!/bin/bash
# Builds custom Docker images from Dockerfiles.
# Dockerfiles are loaded from docker-images/<image name>/<tag>.
# The list of images is defined in the corresponding .list file.

mapfile -t images < $(basename "$0" .sh).list
for image in "${images[@]}"
do
  path=${image%:*}
  tag=${image#*:}
  echo $image $path $tag
  docker build -t "local-$image" "docker/$path/$tag"
  docker tag "local-$image" "local-$path"
done

docker system prune --force
